FROM webratio/nodejs-http-server
WORKDIR /app
COPY . .
EXPOSE 80
CMD [ "http-server", "-p", "80" ]
