'use strict';

var map = angular.module('myApp.map', ['ngRoute']);

map.controller('MapCtrl', function ($scope, userService, infoService ) {

    $scope.user = userService.User;
    $scope.selectedPage = 1;
    $scope.project = null;
    $scope.likes = [];
    $scope.comments = [];
    $scope.expertLikes = [];
    $scope.iframeUrl = null;

    getMap();
    function getMap(){
        $scope.loading = true;
        userService.getMap().then(function(url){
            $scope.iframeUrl = url;
            console.log(url)

            $('#iframe').attr('src',url);
            //$("#iframe").attr("src", url);
            $scope.loading = false;
            tryDigest();
        });
    }

    tryDigest();

    $scope.loading = false;


    function tryDigest() {
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }

});